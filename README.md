 # Curso de Base de Datos


## Programa del Curso
* [Programa del Curso de Base de Datos](/resources/pdf/programa_curso_base_de_datos.pdf "Programa del Curso")
* [Conograma del Curso](/resources/planificacion_sem_1_2023.ods "Cronograma del Curso")

## Proyecto
* Pauta de Evaluación [ [ODP](/resources/proyecto/evaluaci%C3%B3n.odt) | [PDF](/resources/proyecto/evaluaci%C3%B3n.pdf ) ] 
* Avance  - `12 de Mayo`  [ [ODP](/resources/proyecto/templata_entrega_avance.odt) | [PDF](/resources/proyecto/templata_entrega_avance.pdf) ]
 
* Presentación Final - `21 al 30 de Junio` [ [ODP](/resources/proyecto/Presentacion.odt) | [PDF](/resources/proyecto/Presentacion.pdf) ] 


## Clases
**Solemne 1**

1. Introducción [ [ODP](/resources/clases/bdd_1_Introducci%C3%B3n.odp) | [PDF](/resources/clases/bdd_1_Introducci%C3%B3n.pdf ) ]
2. Características generales de las BDD [ [ODP](/resources/clases/bdd_2_%20Caracter%C3%ADsticas%20generales%20de%20las%20BDD.odp) | [PDF](/resources/clases/bdd_2_%20Caracter%C3%ADsticas%20generales%20de%20las%20BDD.pdf) ]
3. Modelamiento de BDD - Parte 1 [ [ODP](/resources/clases/bdd_3_%20modelamiento_base_de_datos_parte1.odp) | [PDF](/resources/clases/bdd_3_%20modelamiento_base_de_datos_parte1.pdf) ]
4. Modelamiento de BDD - Parte 2 [ [ODP](/resources/clases/bdd_4_%20modelamiento_base_de_datos_parte2.odp) | [PDF](/resources/clases/bdd_4_%20modelamiento_base_de_datos_parte2.pdf) ]
5. Modelamiento de BDD - Parte 3 [ [ODP](/resources/clases/bdd_5_%20modelamiento_base_de_datos_parte3.odp) | [PDF](/resources/clases/bdd_5_%20modelamiento_base_de_datos_parte3.pdf) ]
6. Modelamiento de BDD - Parte 4 - Ejercicios [ [ODP](/resources/clases/bdd_6_Ejercicios.odp) | [PDF](/resources/clases/bdd_6_Ejercicios.pdf) ]
7. Modelamiento de BDD - De ER a SQL parte I  [ [ODP](/resources/clases/bdd_7_de_er_a_sql_parte_1.odp) | [PDF](/resources/clases/bdd_7_de_er_a_sql_parte_1.pdf) ]
8. Modelamiento de BDD - De ER a SQL parte II + Agebra Relacional Parte I - SELECT  [ [ODP](/resources/clases/bdd_8_de_er_a_sql_parte_2.odp) | [PDF](/resources/clases/bdd_8_de_er_a_sql_parte_2.pdf) ]
9. Algebra relacional y SQL parte II - SELECT  [ [ODP](/resources/clases/bdd_9_algebra_relacional_y_sql_part_ii.odp) | [PDF](/resources/clases/bdd_9_algebra_relacional_y_sql_part_ii.pdf) ]
10.  Algebra relacional y SQL parte III - SELECT  [ [ODP](/resources/clases/bdd_10_algebra_relacional_y_sql_part_iii.odp) | [PDF](/resources/clases/bdd_10_algebra_relacional_y_sql_part_iii.pdf) ] 
11.  SQL parte IV - Teorema de Mishi  [ [ODP](/resources/clases/bdd_11_algebra_relacional_y_sql_part_iv.odp) | [PDF](/resources/clases/bdd_11_algebra_relacional_y_sql_part_iv.pdf) ] 
12. Preparación para la Solemne 1 [ [ODP](/resources/clases/bdd_12_preparacion_solemne.odp) | [PDF](/resources/clases/bdd_12_preparacion_solemne.pdf) | [ZIP](/resources/clases/bdd_12_preparacion_solemne.zip) ]

13. Entrenamiento Solemne 1  [ [ZIP](/resources/clases/bdd_13_repaso.zip) ]
14. Pruebas Pasadas - Solemne 1  [ [ZIP](/resources/clases/PruebasPasadas.zip) ]
15. SQL parte V - Tecnica de la Fusión  [ [ODP](/resources/clases/bdd_12_algebra_relacional_y_sql_part_v.odp) | [PDF](/resources/clases/bdd_12_algebra_relacional_y_sql_part_v.pdf) ] 
16. Pauta Solemne 1  [ [ODP](/resources/clases/bdd_14_pauta_solemne_1.odp)  || [PDF](/resources/clases/bdd_14_pauta_solemne_1.pdf)| [ [ZIP](/resources/clases/sole1.zip) ]
17. Normalización Parte 1 [[ODP](/resources/clases/bdd_15_Noramalizacion_parte_1.odp)] | [[PDF](/resources/clases/bdd_15_Noramalizacion_parte_1.pdf)]
18. Normalización Parte 2 [[ODP](/resources/clases/bdd_16_normalizacion.odp) ]| [[PDF](/resources/clases/bdd_16_normalizacion.pdf)]
19. Normalización Parte 3 [ [ODP](/resources/clases/bdd_17_normalizacion.odp) ]| [[PDF](/resources/clases/bdd_17_normalizacion.pdf) ]
20. SQL Level 2 [ [ODP](/resources/clases/bdd_17_2_extra_sql.odp) | [PDF](/resources/clases/bdd_17_2_extra_sql.pdf) ]
21. Taller - Paso a Pase de Como crear una base de datos [[ZIP](/resources/clases/bdd_18_insert_update.zip) | [ODP](/resources/clases/bdd_18_insert_update/bdd_18_insert_update.odp) ]| [[PDF](/resources/clases/bdd_18_insert_update/bdd_18_insert_update.pdf) | [todos los archivos](/resources/clases/bdd_18_insert_update/) ]] 
22. Aspectos de Diseño [[ODP](/resources/clases/bdd_19_aspectos_de_diseno.odp) ]| [[PDF](/resources/clases/bdd_19_aspectos_de_diseno.pdf)]
23. Prepración para la Solemne II - part 1 [[ODP](/resources/clases/bdd_20.odp) ]| [[PDF](/resources/clases/bdd_20.pdf)]
24. Prepración para la Solemne II - part 2 [[ODP](/resources/clases/bdd_21.odp) ]| [[PDF](/resources/clases/bdd_21.pdf)]
25. Prepración para la Solemne II - part 3[ [TXT](/resources/clases/sole2_2022/sole2_querys.txt) | [PDF](/resources/clases/sole2_2022/Solemne%202%20-%20Base%20de%20Datos.pdf) ]
26. Prepración para la Solemne II - part 4 -Ejercicios Extra [[ODP](/resources/clases/bdd_22.odp) ]| [[PDF](/resources/clases/bdd_22.pdf)]
27. Prepración para la Solemne II - part 5 -Ejercicios Extra [[ODP](/resources/clases/bdd_23.odp) ]| [[PDF](/resources/clases/bdd_23.pdf)]
29. Prepración para la Solemne II - part 6 -Ejercicios Extra [[TXT](/resources/clases/Ejercicios_solemne2.txt) ]]
30. Pauta Solemne 2 [Files](https://gitlab.com/l30bravo/db_udp/-/tree/main/resources/clases/sole2_2022)

## Apuntes
* [¿Como instalar Postgres?](postgres_install.md)
* [Actualizar password de Postgres](postgres_update_pass.md)
* [¿Como instalar PGModeler?](https://gitlab.com/l30bravo/db_udp/-/blob/main/resources/scripts/pg_modeler_install.sh)


## Tools
* [Draw.io](https://github.com/jgraph/drawio-desktop) - Software para modelar
* [DBeaver](https://dbeaver.io/) - UI para consultar DBs (PostgreSQL, MySQL, SQLite, Oracle, etc)
* [PGModeler](https://github.com/pgmodeler/pgmodeler) - UI para modelar y consultar DBs Postgre (crea el modelo de datos e implemta el modelo en una DB Postgre)
* [PGModeler - Instalador para Archlinux](/resources/scripts/pg_modeler/pg_modeler_install_archlinux.sh) - Instalador de PGModeler para Archlinux)
* [PGModeler - Instalador para Debian / Ubuntu o deribados](/resources/scripts/pg_modeler/pg_modeler_install_ubuntu.sh) - Instalador de PGModeler para Debian / Ubuntu y deribados)
## Bibliografia
* [Introducción a los sistemas de Base de Datos](/resources/pdf/Introduccion%20a%20los%20Sistemas%20de%20Bases%20de%20Datos%20-%207ma%20Edicion%20-%20C.%20J.%20Date.pdf "Introducción a los sistemas de Base de Datos")

* [Manual de PostgreSQL](/resources/pdf/Postgres-User.pdf "Manual de PostgreSQL")
